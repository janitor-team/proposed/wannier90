#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export LIBDIR = /usr/lib/$(DEB_TARGET_MULTIARCH)

%:
	dh $@ --no-parallel

override_dh_auto_configure:
	cp config/make.inc.gfort.dynlib make.inc
	find test-suite -type f | sort > debian/test-suite.lst
	dh_auto_configure

override_dh_auto_build:
	mkdir --parents utility/w90pov/obj
	dh_auto_build -- all doc
	cd utility/w90pov/doc/ && FORCE_SOURCE_DATE=1 pdflatex w90pov.tex
	make -C utility/w90vdw/doc

override_dh_auto_install:
	dh_auto_install -- LIBDIR=/usr/lib/$(DEB_TARGET_MULTIARCH)

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	PATH=$(CURDIR):${PATH} make test-serial
endif

override_dh_installexamples-indep:
	dh_install -p wannier90-data examples usr/share/doc/wannier90

override_dh_installchangelogs:
	dh_installchangelogs CHANGELOG.md

override_dh_auto_clean:
	test -e make.inc && dh_auto_clean -- veryclean || true
	make -C doc/tutorial/ veryclean
	make -C doc/user_guide/ veryclean
	make -C utility/w90vdw/doc veryclean
	rm -f doc/tutorial/tutorial.bbl doc/tutorial/tutorial.blg doc/tutorial/tutorial.out
	rm -f utility/w90pov/doc/w90pov.aux
	rm -f utility/w90pov/doc/w90pov.log
	rm -f utility/w90pov/doc/w90pov.out
	rm -f utility/w90pov/doc/w90pov.pdf
	rm -f utility/w90pov/doc/w90povNotes.bib
	rm -f make.inc *.x libwannier.a
	-test -e debian/test-suite.lst && find test-suite -type f | sort \
		| comm -1 -3 debian/test-suite.lst - | xargs rm -f
	rm -f debian/test-suite.lst
